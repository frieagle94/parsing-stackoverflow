# Parsing StackOverflow

The project aims to scan StackOverflow posts to get some of most common problems for some technologies of interest and, eventually, the solution.

**WARNING**
The project requires:
- a csv file named as [path][posts] in config.ini containing the posts dataset. The csv can be obtained by querying https://data.stackexchange.com/stackoverflow/query/new , the query in sql format and the schema of the interested table in SO db are placed in /misc dir. The csv file must be placed at /parsing/input and include following columns at least:
```
Id, Title, Body, AcceptedAnswerId, ParentId, QuestionId

```

- A Word2Vec model pretrained on S0 data. The .bin file can downloaded from http://doi.org/10.5281/zenodo.1199620 (Credits to https://github.com/vefstathiou/SO_word2vec) and must be placed in the root directory of project.

Please use the _config.ini_ file to customize settings such as resource paths and to choose the steps to execute.
Execution log will be written at _scan_so.log_ in the project's root directory.

**WHAT DOES THE SCRIPT DO?**
The script is implemented in Python and it is composed by following main steps:

- Parse threads into json from csv exported by querying https://data.stackexchange.com/stackoverflow/query/new; 
- Dump threads to json in format:
```
thread_obj:

{
    'Id': "thread's id*
    'Title': "thread's title on SO"
    'Body': "thread's content parsed in plaintext"
    'Children' : children_obj
}

children_obj:

{
    'Body': "thread answer's parsed in plaintext",
    'Children': children_obj
}

```
The thread's answer is represented by a plaintext string which is placed in 'Children' attribute of the last answer not accepted.
```
last_children_obj:

{
    'Body': "thread last answer's not accepted parsed in plaintext",
    'Children': accepted answer's body
}
```
- Clean bodies of thread's posts, please refer to /cleaning directory
- Compute topics of threads with LDA, please refer to /topics directory
```
Topic #1: w1, w2, w3, ..., word10
...
Topic #20: w1, w2, w3, ..., word10
```
- Embed words of sentences composing the bodies of thread's posts with Word2Vec, please refer to /embedding folder
- Reduce the dataset dimensionality with PCA
- Perform elbow method and silhouette analysis to achieve the best K to comput K-means
- Cluster the threads with K-Means algorithm eventually
- Dump clusters of threads: the result is a json file containing the clusters and their threads in this format:
```
{
    'cluster_id' : [
        thread_obj,
        thread_obj,
        thread_obj,
    ],
    'cluster_id' : [...],
    ...
}
```

- Compute clusters stats such as thread count, most and mean frequent words in cluster: the result is a json file containing the clusters' stats:
```
{
    'cluster_id' : {
        'thread_count': number of threads in cluster
        'best': {
            'word1': number of occurrences of word1 in cluster,
            ...,
            'word10': ...
        }
        'mean': {
            'word1': number of occurrences of word1 in cluster,
            ...,
            'word10': ...
        }
    },
    'cluster_id' : {...},
    ...
}
```
- Filter clusters and clusters stats dumping by matching a condition based on topics extracted with LDA. The condition is recursevely defined as:

if _topic_ is a topic, _fun(topic)_ is a condition that could be:
- _start(topic)_ is a condition which matches all words starting with _topic_
- _sub(topic)_ is a condition which matches all words that contain _topic_
- _whole(topic)_ is a condition which matches all words that are equal to _topic_

if _fun1(topic)_ and _fun2(topic)_ are conditions, _fun1(topic) and fun2(topic)_ is a condition

if _fun1(topic) and fun2(topic)_ and _fun3(topic) and fun4(topic)_ are conditions, _fun1(topic) and fun2(topic) or fun3(topic) and fun4(topic)_ is a condition
