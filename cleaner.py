# Import useful system modules
import json
import re
import string
import os

# Import useful library modules
import pandas as pd
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

# Import custom modules
import logger
from config import PATHS, HEARTBEAT

def remove_stopwords(sentence, additional_stopwords = []):
    """ Performs the stopwords removing
    
    Parameters
     ----------
     sentence : list of words to filter
      
    Returns
     -------
     filtered_sentence: list of words without stopwords
    """

    # Get stopwords form library and add some customs
    stopwords_verbs = ['say', 'get', 'go', 'know', 'may', 'need', 'like', 'make', 'see', 'want', 'come', 'take', 'use', 'would', 'can', 'cant', 'could' , 'couldnt', 'lets', ]
    stopwords_other = ['one', 'mr', 'bbc', 'image', 'getty', 'de', 'en', 'caption', 'also', 'copyright', 'something', 'dont']
    stopwords_skipped = ['not', 'and', 'this', 'that', 'for', 'here', 'what', 'from', 'but', 'are', 'you', 'heres', 'didnt', 'theres', 'isnt', 'thats', 'cant', 'doesnt', 'whats', 'youre', 'wont']
    all_stopwords = stopwords.words('English') + stopwords_verbs + stopwords_other + stopwords_skipped + additional_stopwords

    filtered_sentence = [word for word in sentence if not word in all_stopwords]

    return filtered_sentence

def remove_digits(sentence):
    """ Performs the words cointaining digits removing
    
    Parameters
     ----------
     sentence : list of words to filter
      
    Returns
     -------
     filtered_sentence: list of words without digits
    """

    filtered_sentence = [word for word in sentence if not any(char.isdigit() for char in word)]

    return filtered_sentence

def clean_body_for_dumping(body):
    """ Clean the leaf node body
    
    Parameters
     ----------
     body: leaf node's body
      
    Returns
     -------
     body: leaf node's body cleaned
    """
    body = re.sub('\n',' ', body)
    body = re.sub(' +',' ', body)
    return body

def clean_dict_for_dumping(log, threads):
    """ Flatten the thread by recursively get childrens of post
    
    Parameters
     ----------
     log: log buffer
     threads: threads dict
      
    Returns
     -------
     None
    """

    logger.log_message(log, 'Start of threads cleaned dump execution.')

    if not os.path.exists(PATHS['cleaning-folder']):
        os.makedirs(PATHS['cleaning-folder'])
        logger.log_message(log, "Directory " + PATHS['cleaning-folder'] + " does not exist. It will be created.")


    output_path = PATHS['cleaning-folder'] + PATHS['threads-cleaned-for-dumping-json']

    for thread in threads:
        
        threads[thread]['Body'] = clean_body_for_dumping(threads[thread]['Body'])
        threads[thread]['Children'] = clean_children_dor_dumping(threads[thread]['Children'])

        # Write heartbeat on log
        if int(thread) > 0 and int(thread)%HEARTBEAT['rebuild-base-data'] == 0 :
            logger.log_message(log, 'Heartbeat: ' + thread + ' threads cleaned and dumped.')

    with open(output_path, "w+", encoding="utf-8") as output:
        json.dump(threads, output, indent=4)
        output.close()

    logger.log_message(log, 'End of cleaning execution: cleaned threads has been dumped to ' + output_path)

    return None

def clean_children_dor_dumping(childrens):
    """ Clean thread's childrens by recursively get their childrens
    
    Parameters
     ----------
     childrens: childrens obj
      
    Returns
     -------
     anonymous: cleaned childrens body obj
    """
    if type(childrens) is str:
        return clean_body_for_dumping(childrens)
    else:
        return {
            'Body': clean_body_for_dumping(childrens['Body']),
            'Children': clean_children_dor_dumping(childrens['Children'])
        }

def clean_df_for_dumping(log, threads_pd, columns_to_clean = [], columns_to_drop = []):
    """ Performs the data cleaning for dumping clusters
    
    Parameters
     ----------
     log : log buffer
     threads_pd : threads DF
     columns_to_clean: columns to clean names
     columns_to_drop: columns to drop names
      
    Returns
     -------
     None
    """

    output_path = PATHS['cleaning-folder'] + PATHS['threads-cleaned-for-dumping-csv']

    if not os.path.exists(PATHS['cleaning-folder']):
        os.makedirs(PATHS['cleaning-folder'])
        logger.log_message(log, "Directory " + PATHS['cleaning-folder'] + " does not exist. It will be created.")

    for column_to_clean in columns_to_clean: 

        logger.log_message(log, 'Performing data cleaning to dumping for column ' + column_to_clean + '...')

        # Replace multiple spaces with a single one
        threads_pd[column_to_clean] = threads_pd[column_to_clean].map(lambda x: clean_body_for_dumping(x))

        logger.log_message(log, 'Column ' + column_to_clean + ' cleaned for dumping.')

    # Drop not useful columns
    threads_pd = threads_pd.drop(columns=columns_to_drop, errors='ignore')

    with open(output_path, "w+", encoding="utf-8") as output:
        threads_pd.to_csv(output)

    logger.log_message(log, 'Data for dumping cleaned.')

def clean_data_for_embedding(log, threads_pd, columns_to_clean = [], columns_to_drop = [], additional_stopwords = []):
    """ Performs the data cleaning
    
    Parameters
     ----------
     log : log buffer
     threads_pd : threads DF
     columns_to_clean: columns to clean names
     columns_to_drop: columns to drop names
     additional_stopwords: additional stopwords
      
    Returns
     -------
     threads_pd: cleaned threads_pd
    """

    output_path = PATHS['cleaning-folder']

    if not os.path.exists(output_path):
        os.makedirs(output_path)
        logger.log_message(log, "Directory " + output_path + " does not exist. It will be created.")

    if len(additional_stopwords) > 0:
        output_path += 'additional_'

    output_path += PATHS['threads-cleaned']

    nltk.download('stopwords')

    # Drop columns
    threads_pd_cleaned = threads_pd.drop(columns=columns_to_drop, errors='ignore')

    # Clean columns
    for column_to_clean in columns_to_clean:

        logger.log_message(log, 'Performing data cleaning for column ' + column_to_clean + '...')

        # Remove punctuation
        logger.log_message(log, 'Remove punctuation...')
        threads_pd_cleaned[column_to_clean] = threads_pd_cleaned[column_to_clean].map(lambda x: re.compile('[%s]' % re.escape(string.punctuation)).sub(' ', x))

        # Convert titles to lowercase
        logger.log_message(log, 'Convert titles to lowercase...')
        threads_pd_cleaned[column_to_clean] = threads_pd_cleaned[column_to_clean].map(lambda x: x.lower())

        # Remove word of length < 3
        logger.log_message(log, 'Remove word of length < 3...')
        threads_pd_cleaned[column_to_clean] = threads_pd_cleaned[column_to_clean].map(lambda x: re.compile(r'\W*\b\w{1,2}\b').sub('', x))

        # Replace multiple spaces with a single one
        logger.log_message(log, 'Replace multiple spaces with a single one...')
        threads_pd_cleaned[column_to_clean] = threads_pd_cleaned[column_to_clean].map(lambda x: re.sub(' +',' ', x))

        # Remove links as word
        logger.log_message(log, 'Remove links as word...')
        threads_pd_cleaned[column_to_clean] = threads_pd_cleaned[column_to_clean].map(lambda x: re.sub(r'\w*http\w*', '', x))

        # Tokenization
        logger.log_message(log, 'Tokenization...')
        threads_pd_cleaned[column_to_clean] = threads_pd_cleaned[column_to_clean].map(lambda sentence: word_tokenize(sentence))

        # Remove only digits words
        logger.log_message(log, 'Remove only digits words...')
        threads_pd_cleaned[column_to_clean] = threads_pd_cleaned[column_to_clean].map(lambda sentence: remove_digits(sentence))

        # Removing stopwords
        logger.log_message(log, 'Removing stopwords...')
        threads_pd_cleaned[column_to_clean] = threads_pd_cleaned[column_to_clean].map(lambda sentence: remove_stopwords(sentence, additional_stopwords))

        logger.log_message(log, 'Column ' + column_to_clean + ' cleaned.')

    with open(output_path, "w+", encoding="utf-8") as output:
        threads_pd_cleaned.to_csv(output)

    logger.log_message(log, 'Data cleaned.')

    return threads_pd_cleaned