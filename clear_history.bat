echo Delete all previously computed files...
echo Delete parsing results...
del ".\parsing\results\*.*" /s /f /q
echo Delete cleaning results...
del ".\cleaning\*" /s /f /q
echo Delete embedding results...
del ".\embedding\*" /s /f /q
echo Delete topics results...
del ".\topics\*" /s /f /q
echo Delete clustering results...
del ".\clustering\*.json" /s /f /q
del ".\clustering\clusters\*" /s /f /q
del ".\clustering\clusters\*" /s /f /q
del ".\clustering\stats\filtered\*" /s /f /q
del ".\clustering\dumps\*.json" /s /f /q
del ".\clustering\dumps\filtered\*.json" /s /f /q
echo Done!