# Import useful system modules
import json
import re

# Import useful library modules
import numpy as np
import pandas as pd
import sklearn.cluster as cluster
import matplotlib.pyplot as plt
import pylab

# Import custom modules
import logger
from config import PATHS, SETTINGS, HEARTBEAT

def pca_reduct(log, X, components):
    """ Performs the PCA features reduction
    
    Parameters
     ----------
     log : log buffer
     X : dataset
     components : number of main components
      
    Returns
     -------
     pca_X : dataset reduced

    """

    logger.log_message(log, 'Doing the PCA reduction with ' + str(components) + ' main components...')

    from sklearn.decomposition import PCA
    pca_model = PCA(n_components=components)
    pca_X = pca_model.fit_transform(X)

    if SETTINGS['open-plots']:
        logger.log_message(log, 'Opening the PCA reduction plot...')

        pylab.scatter(pca_X[:, 0], pca_X[:, 1], 20)
        pylab.show()
        
        logger.log_message(log, 'PCA reduction plot closed.')

    return pca_X

def k_analysys(log, X, max_clusters):
    """ Performs the PCA features reduction
    
    Parameters
     ----------
     log : log buffer
     X : dataset
     max_clusters : number of clusters to compute metrics
      
    Returns
     -------
     cluster_max_sil : best K because of max silhouette
     
    """

    logger.log_message(log, 'Looking for the best K with elbow method and silhouette analysis...')
    logger.log_message(log, 'Computing metrics for max ' + str(max_clusters) + ' clusters...')

    from sklearn.metrics import silhouette_score

    max_clusters += 1

    sse = []
    sil = []

    # For each possible number of clusters
    for k in range(1,max_clusters+1):

        # Build the Kmeans model and launch it
        model = cluster.KMeans(n_clusters=k, random_state=0, verbose= 1)
        model.fit(X)

        # Compute the SSE for elbow method, only in possible clusters range
        if k < max_clusters:
            sse.append(model.inertia_)

        # Compute the silhouette score, only in possible clusters range +1
        if k > 1:
            labels = model.labels_
            sil.append(silhouette_score(X, labels, metric = 'euclidean'))
        
        logger.log_message(log, 'Computed measures with ' + str(k) + ' clusters.')

    logger.log_message(log, 'Elbow method results:')
    for index, res in sse:
        logger.log_message(log, 'Cluster #: ' + str(index+1) + '; SSE: '  + str(res))

    logger.log_message(log, 'Silhouette results:')
    max_sil = 0
    cluster_max_sil = 0
    for index, res in sil:
        if res > max_sil:
            max_sil = res
            cluster_max_sil = index
        logger.log_message(log, 'Cluster #: ' + str(index+2) + '; Silhouette: '  + str(res))

    if SETTINGS['open-plots']:
        logger.log_message(log, 'Opening elbow method plot...')

        plt.style.use("fivethirtyeight")
        plt.plot(range(1,max_clusters), sse)
        plt.xticks(range(1,max_clusters))
        plt.xlabel("Cluster #")
        plt.ylabel("SSE")
        plt.show()

        logger.log_message(log, 'Elbow method plot closed.')
        logger.log_message(log, 'Opening silhouette plot...')

        plt.style.use("fivethirtyeight")
        plt.plot(range(2,max_clusters+1), sil)
        plt.xticks(range(2,max_clusters+1))
        plt.xlabel("Cluster #")
        plt.ylabel("Silhouette")
        plt.show()

    logger.log_message(log, 'Silhouette plot closed.')

    return cluster_max_sil

def compute_clusters(log, X, K):
    """ Performs the K-Means algorithm
    
    Parameters
     ----------
     log : log buffer
     X : dataset
     K : number of clusters
      
    Returns
     -------
     result_obj : dict of clusters with theirs ids
     
    """
    
    logger.log_message(log, 'Executing KMEANS with K = ' + str(K) + '...')

    model = cluster.KMeans(n_clusters=K, random_state=0)    
    result = model.fit_predict(X)

    logger.log_message(log, 'KMEANS execution ended.')
    logger.log_message(log, 'Going to print and plot results...')

    if SETTINGS['open-plots']:
        colors = ["b","g","r","c","m","y","k","w"]
        markers = ['o', 'v', '^', '<', '>', '8', 's', 'p', 'h', 'H', 'D', 'd', 'P', 'X']
        
    result_obj = {}

    for i in range(K):
        result_obj[str(i)] = []

        if SETTINGS['open-plots']:
            # Plot the clusters
            plt.scatter(
                X[result == i, 0], X[result == i, 1],
                s=50, c=colors[i%len(colors)],
                marker=markers[i%len(markers)], edgecolor='black',
                label='cluster ' + str(i+1)
            )

    filename = PATHS['cluster-prefix'] + str(K) + "K_" + str(np.shape(X)[1]) + "comp.json"
    output_path = PATHS['clusters-folder'] + filename

    with open(output_path, "w+") as output:
        for index, res in enumerate(result):
            result_obj[str(res)].append(index)
        json.dump(result_obj, output, indent=4)
        output.close()

    if SETTINGS['open-plots']:
        # Plot the centroids
        plt.scatter(
            model.cluster_centers_[:, 0], model.cluster_centers_[:, 1],
            s=250, marker='*',
            c='yellow', edgecolor='black',
            label='centroids'
        )
        plt.legend(scatterpoints=1)
        plt.grid()
        plt.show()

    logger.log_message(log, 'K-MEANS results has been saved in ' + output_path)

    return result_obj

def dump_threads_of_cluster(log, cluster_dump, threads, clusters = None, is_filtered_clusters = False):
    """ Get the parsed posts and print them for each clusters
    
    Parameters
     ----------
     log : log buffer
     cluster_dump : path to cluster dict as json
     threads : posts cleaned
     clusters : clusters dict (Default = none)
     is_filtered_clusters : True if clusters is filtered
      
    Returns
     -------
     None
     
    """

    clusters_path = PATHS['clusters-folder'] + cluster_dump
    if not is_filtered_clusters:
        dump_path = PATHS['dump-folder'] + PATHS['clusters-dump-prefix'] + cluster_dump
    else:
        dump_path = PATHS['dump-filtered-folder'] + PATHS['clusters-dump-prefix'] + "filtered_" + cluster_dump

    logger.log_message(log, 'Clusters dump started.')
    
    logger.log_message(log, 'Retrieving clusters...')

    if clusters is None:
        with open(clusters_path, "r+") as cluster_file:
            clusters = json.load(cluster_file)
            cluster_file.close()

    logger.log_message(log, 'Dump json writing started...')

    with open(dump_path, 'w+') as output:
    
        clusters_to_dump = {}

        for cluster in clusters:

            clusters_to_dump[cluster] = []

            for thread_index in clusters[cluster]:

                thread = {
                    'Title': threads[str(thread_index)]['Title'],
                    'Body': threads[str(thread_index)]['Body'],
                    'Children': threads[str(thread_index)]['Children']
                }

                clusters_to_dump[cluster].append(thread)

        json.dump(clusters_to_dump, output, indent=4)
        output.close()

    logger.log_message(log, 'Clusters dumped.')

def dump_stats_of_cluster(log, cluster_dump, threads_pd, clusters = None):
    """ Get the parsed posts and print 10 most and least frequent words in each cluster
    
    Parameters
     ----------
     log : log buffer
     cluster_dump : path to cluster dict as json
     threads_pd : threads pd
     clusters : clusters dict (Default = none)
      
    Returns
     -------
     None
     
    """
    clusters_to_stats_path = PATHS['clusters-folder'] + cluster_dump
    clusters_stats_path = PATHS['stats-folder'] + PATHS['clusters-stats-prefix'] + cluster_dump
    clusters_stats_filtered_path = PATHS['stats-filtered-folder'] + PATHS['clusters-stats-prefix'] + cluster_dump

    logger.log_message(log, 'Clusters stats dump started.')
    
    logger.log_message(log, 'Retrieving clusters...')

    if clusters is None:
        with open(clusters_to_stats_path, "r+") as cluster_file:
            clusters = json.load(cluster_file)
            cluster_file.close()

    stats = {}
    words_under_mean = compute_words_count(log, threads_pd['Body'])

    logger.log_message(log, 'Dump stats writing started...')

    for cluster in clusters:
               
        words = {}
        for post_index in clusters[cluster]:
            for word in threads_pd.iloc[post_index]['Body']:
                if word in words_under_mean:
                    if word in words:
                        words[word] += 1
                    else:
                        words[word] = 1

        if len(words) > 0:

            most_common_words = {}
            mean_common_words = {}

            sorted_words = {word: count for word, count in sorted(words.items(), key=lambda item: item[1], reverse=True)}

            word_items = list(sorted_words.items())

            for word, count in word_items[:10]:
                most_common_words[word] = count

            max_frequent = list(sorted_words.values())[0]
            min_frequent = list(sorted_words.values())[len(sorted_words)-1]
            mean_frequent = round(((max_frequent+min_frequent)/2)+1)
            mean_index = 0
            if min_frequent != max_frequent:
                while (mean_index == 0): 
                    for index, item in enumerate(word_items):
                        if int(sorted_words[item[0]]) == mean_frequent:
                            mean_index = index
                            break
                    mean_frequent -= 1
                    if mean_frequent == 0:
                        mean_frequent = 1
                for word, count in word_items[mean_index:mean_index+10]:
                    mean_common_words[word] = count

            words = {}
            words['thread_count'] = len(clusters[cluster])
            words['best'] = most_common_words
            words['mean'] = mean_common_words

            stats[cluster] = words

        # Write heartbeat on log
        if int(cluster) > 0 and int(cluster)%HEARTBEAT['dump-filtered-clusters'] == 0 :
            logger.log_message(log, 'Heartbeat: ' + str(cluster) + ' clusters computed.')

    with open(clusters_stats_path, 'w+') as output:
        json.dump(stats, output, indent=4)
        output.close()

    logger.log_message(log, 'Stats dumped.')

    logger.log_message(log, 'Filtering and sorting stats started.')

    sorted_clusters = sorted(stats, key=lambda row: stats[row].get('thread_count', 0), reverse=True)

    mean_thread_count = None
    if SETTINGS['get-over-mean-clusters']:
        mean_thread_count = 0
        for cluster in sorted_clusters:
            mean_thread_count += stats[cluster]['thread_count']
        mean_thread_count /= len(sorted_clusters)
        sorted_clusters = [row for row in sorted_clusters if stats[row].get('thread_count', 0) > mean_thread_count]

    logger.log_message(log, 'Dump sorted and filtered stats...')
    
    import topics_matcher as matcher
    filtered_stats = {}
    for cluster in sorted_clusters:
        cluster_stats = stats[cluster]
        if matcher.check_words_matching([word for word in cluster_stats['mean']] + [word for word in cluster_stats['best']]):
            filtered_stats[cluster] = cluster_stats

    with open(clusters_stats_filtered_path, "w+") as output:
        json.dump(filtered_stats, output, indent=4)
        output.close()

    logger.log_message(log, 'Filtered and sorted stats dumped.')

def compute_words_count(log, threads):
    """ Get all words in dataset and take only words with under the median occurrences
    
    Parameters
     ----------
     log: log buffer
     threads : dataset
      
    Returns
     -------
     sorted_words_under_median: words under the median
     
    """    

    logger.log_message(log, 'Counting words started...')

    output_path = PATHS['clustering-folder'] + PATHS['words-count']
    filtered_output_path = PATHS['clustering-folder'] + PATHS['filtered-words-count']

    words_count = {}

    for thread in threads:
        for word in thread:
            if word in words_count:
                words_count[word] += 1
            else:
                words_count[word] = 1

    sorted_words = {word: count for word, count in sorted(words_count.items(), key=lambda item: item[1], reverse=True)}

    with open(output_path, "w+", encoding='utf-8') as word_count_file:
        json.dump(sorted_words, word_count_file, indent=4)
        word_count_file.close()

    filtered_sorted_words = {word: sorted_words[word] for word in sorted_words if sorted_words[word] < 5000 and sorted_words[word] > 50 }

    with open(filtered_output_path, "w+", encoding='utf-8') as filtered_word_count_file:
        json.dump(filtered_sorted_words, filtered_word_count_file, indent=4)
        filtered_word_count_file.close()

    max_word_count = 0
    min_word_count = 100000000
    median_word_count = 0
    for word in words_count:
        if words_count[word] > max_word_count:
            max_word_count = words_count[word]
        if words_count [word] < min_word_count:
            min_word_count = words_count[word]
    median_word_count = (max_word_count + min_word_count)/2
    
    filtered_words_count = [word for word in words_count if words_count[word] < median_word_count] #mean_word_count]

    logger.log_message(log, 'Counting words ended.')

    return filtered_words_count

def dump_filtered_stats_of_cluster(log, cluster_dump, threads, clusters = None):
    """ Get the parsed posts and print them for each filtered clusters
    
    Parameters
     ----------
     log : log buffer
     cluster_dump : path to cluster dict as json
     threads : threads cleaned
     clusters : clusters dict (Default = none)
      
    Returns
     -------
     None
     
    """
    clusters_path = PATHS['clusters-folder'] + cluster_dump
    clusters_stats_filtered_path = PATHS['stats-filtered-folder'] + PATHS['clusters-stats-prefix'] + cluster_dump

    logger.log_message(log, 'Filtered dump started.')
    
    if clusters is None:
        with open(clusters_path, "r+") as cluster_file:
            clusters = json.load(cluster_file)
            cluster_file.close()

    with open(clusters_stats_filtered_path, "r+", encoding="utf-8") as cluster_filtered_file:
        filtered_clusters = json.load(cluster_filtered_file)
        cluster_filtered_file.close()

    for cluster in filtered_clusters:
        filtered_clusters[cluster] = clusters[cluster]

    dump_threads_of_cluster(log, cluster_dump, threads, filtered_clusters, is_filtered_clusters=True)