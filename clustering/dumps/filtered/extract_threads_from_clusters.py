"""
The following script will dump the interesting threads
Threads dump must be in a file 'threads_dump_filtered_cluster_1000K_2comp.json' in /clustering/dumps/filtered directory.
The list of interesting threads ids in this format
cluster_id:thread_id,thread_id,thread_id... must be in a file 'interesting.txt' in /clustering/dumps/filtered directory.
The filtering result will be placed in a file interesting.json' in /clustering/dumps/filtered directory.
"""

import json

with open("threads_dump_filtered_cluster_1000K_2comp.json", "r+", encoding="utf-8") as clusters_file:
    clusters = json.load(clusters_file)
    clusters_file.close()

with open("interesting.txt", "r+", encoding="utf-8") as interesting_file:
    interesting = interesting_file.readlines()
    clusters_file.close()

threads = {}
for interest in interesting:
    cluster_id = interest.split(":")[0]
    threads_row = interest.split(":")[1]
    threads[cluster_id] = threads_row.split('-')
    threads[cluster_id] = [int(thread)-1 for thread in threads[cluster_id]]

to_dump = []
for cluster_row in threads:
    cluster = clusters[cluster_row]
    for thread in threads[cluster_row]:
        obj = {
            "Cluster": cluster_row,
            "Thread" : clusters[cluster_row][thread]
        }
        to_dump.append(obj)
    interesting_file.close()

with open("interesting.json", "w+", encoding="utf-8") as interesting_file:
    json.dump(to_dump, interesting_file, indent=4)
    interesting_file.close()