# Import useful library modules
from configparser import ConfigParser

CONFIG_PATH = 'config.ini'

# Global variables by config file
HEARTBEAT = {}
SETTINGS = {}
STEPS = {}
LDA_SETTINGS = {}
CLUSTERING_SETTINGS = {}
PATHS = {}
TOPICS_MATCH_CONDITION = {}
LDA_INPUT = {}

def build_topics_match(condition):
    """Build the topics matching condition from config.

    Parameters
     ----------
     condition : condition as string

    Returns
     -------
     None
    """

    or_conditions = condition.split(' or ')

    for index, or_condition in enumerate(or_conditions):

        match_or_condition = []

        and_conditions = or_condition.split(' and ')
        for and_condition in and_conditions:
            match_operator = and_condition.split("(")[0]
            topic = and_condition.split("(")[1][:-1]
            match_and_condition = (match_operator, topic)
            match_or_condition.append(match_and_condition)

        TOPICS_MATCH_CONDITION[index] = match_or_condition

def parse_bool_config(setting, config):
    """Parse a bool configuration by INI config file.

    Parameters
     ----------
     setting : Name of the operation to be switched on /of
     config : String representing the bool value

    Returns
     -------
     True/False : configuration value
    """

    if config == 'true':
        return True
    elif config == 'false':
        return False
    else:
        print('Please enter a true/false value in ' + setting + ' tag.')
        raise ValueError

def read_settings():
    """Parse configuration settings by INI config file.

    Parameters
     ----------

    Returns
     -------
     None
    """

    config = ConfigParser()
    config.read(CONFIG_PATH)

    # Compute roots settings
    settings_root = config['settings']

    for setting in settings_root:
        if setting.startswith('use-best-k'):
            SETTINGS[setting] = parse_bool_config(
                'use-best-k', settings_root[setting])
        elif setting.startswith('open-plots'):
            SETTINGS[setting] = parse_bool_config(
                'open-plots', settings_root[setting])
        elif setting == 'topics-match-condition':
            build_topics_match(settings_root[setting])
        else:
            SETTINGS[setting] = settings_root[setting]

    # Compute heartbeat settings
    heartbeat_root = config['heartbeat-operation']
    for heartbeat in heartbeat_root:
        try:
            HEARTBEAT[heartbeat] = int(heartbeat_root[heartbeat])
        except ValueError:
            print(
                'Please enter an integer value in heartbeat-operation tag configuration.')
            raise

    # Compute step settings
    step_root = config['step']
    for step in step_root:
        STEPS[step] = parse_bool_config('step', step_root[step])

    # Compute lda settings
    lda_root = config['lda-step']
    for setting in lda_root:
        LDA_SETTINGS[setting] = parse_bool_config(
            'lda-step', lda_root[setting])
    lda_root = config['lda-input']
    for lda in lda_root:
        try:
            LDA_INPUT[lda] = int(lda_root[lda])
        except ValueError:
            print('Please enter an integer value in lda-input configuration.')
            raise


    # Compute clustering settings
    clustering_root = config['clustering-step']
    for setting in clustering_root:
        CLUSTERING_SETTINGS[setting] = parse_bool_config(
            'clustering-step', clustering_root[setting])

    # Parse paths
    paths_root = config['paths']
    for path in paths_root:
        PATHS[path] = paths_root[path]