# Import useful system modules
import json
import re
import os

# Import useful library modules
from gensim.models.keyedvectors import KeyedVectors
import csv
import pandas as pd
import numpy as np
import logger

# Import custom modules
from config import PATHS

# Load the prebuilt model
word_vect = KeyedVectors.load_word2vec_format("SO_vectors_200.bin", binary=True)
words_not_vectorized = {}
sentence_not_vectorized = 0

def thread_embed(log, threads_cleaned_pd):
    """ Build thread vector representation by computing word embedding.
    
    Parameters
     ----------
     log : log buffer
     threads_cleaned_pd : cleaned threads DF
    Returns
     -------
     threads_embedded_pd: embedded threads DF
    """

    if not os.path.exists(PATHS['embedding-folder']):
        os.makedirs(PATHS['embedding-folder'])
        logger.log_message(log, "Directory " + PATHS['embedding-folder'] + " does not exist. It will be created.")

    output_path = PATHS['embedding-folder'] + PATHS['threads-embedded']
    dataset_path = PATHS['embedding-folder'] + PATHS['threads-embedded-dataset']

    with open(dataset_path, "w+", encoding="utf-8") as dataset_file:

        dataset_buf = csv.writer(dataset_file, delimiter=';')
        
        logger.log_message(log, 'Building threads vectors...')

        threads_cleaned_pd['Vector'] = threads_cleaned_pd['Body'].map(lambda body: sentence_embed(body))
        threads_cleaned_pd['Vector'].map(lambda vec:dataset_buf.writerow(vec[:200])) 
        dataset_file.close()

    threads_embedded_pd = threads_cleaned_pd.drop(columns=['Body'])

    with open(output_path, "w+", encoding="utf-8") as output:
        threads_embedded_pd.to_csv(output)
        output.close()

    logger.log_message(log, 'Word embedding complete.')
    logger.log_message(log, 'Sentences skipped: ' + str(sentence_not_vectorized))
    logger.log_message(log, 'Words skipped: ' + str(len(words_not_vectorized)))

    # Dump words not existing on dictionary
    with open(PATHS['embedding-folder'] + PATHS['embedding-skipped'], "w+") as skipped_file:
        sorted_skipped = {word: count for word, count in sorted(words_not_vectorized.items(), key=lambda item: item[1], reverse=True)}
        json.dump(sorted_skipped, skipped_file)
        skipped_file.close()
    
    return threads_embedded_pd

def sentence_embed(thread_body):
    """ Build sentence vector representation by computing word embedding.
    
    Parameters
     ----------
     thread_body : thread body words as list
    Returns
     -------
     vec: thread vec representation
    """
    
    # If body's length is > 0
    if len(thread_body) > 0:
        
        sentence_index = 0
        vec = []
        
        # For each word
        for w in thread_body:
            try:
                # At first step, the sentence vector = 1st word vector
                if sentence_index == 0:
                    vec = word_vect.word_vec(w)
                # Cumulate following words
                else:
                    vec = np.add(vec, word_vect.word_vec(w))
                    sentence_index += 1
            # If dict not contains word, log it
            except Exception:
                if w in words_not_vectorized:
                    words_not_vectorized[w] += 1
                else:
                    words_not_vectorized[w] = 1
            
        # Compute the mean vec
        if sentence_index > 0:
            vec = np.asarray(vec)/sentence_index

        return vec
    else:
        return []   