# Import useful system modules
import json
import re
import string
import os

# Import useful library modules
import pandas as pd
import numpy as np

# Import custom modules
import logger
from config import LDA_INPUT, PATHS
import nltk
from nltk.stem import WordNetLemmatizer
from gensim import corpora, models

# Build a lemmatizer
wordnet_lemmatizer = WordNetLemmatizer()

def lemmatizing_sentence(sentence):
	""" Performs the data lemmatization
    
    Parameters
     ----------
     sentence : list of tokens
      
    Returns
     -------
     stemmatized: sentence with lemmatization applied
    """

	lemmatized = [wordnet_lemmatizer.lemmatize(word) for word in sentence]
	return lemmatized

def print_topics_corpora(model, num_topics, output):
    """ Print topics computed by LDA with corpora library
    
    Parameters
     ----------
     model : LDA model
     num_topics : # topics
     output : output buffer
      
    Returns
     -------
     None
    """

    for i,topic in model.show_topics(formatted=True, num_topics=num_topics, num_words=LDA_INPUT['words']):
        words = topic.split('+')
        words = [word.split('*')[1].strip(' "') for word in words]
        output.write('Topic #' + str(i) + ": " + ','.join(words))
        output.write('\n')

def filter_topics():
    """ 
    The following script will filter the words contained in topics based on user defined words
    Raw topics must be in a file 'topics.txt' in the root of script directory.
    The filtering result will be placed in a file filtered_topics.txt' in the root of script directory.
    
    Parameters
     ----------
      
    Returns
     -------
    """

    topics_path = PATHS['topics-folder']

    if not os.path.exists(PATHS['topics-folder']):
        os.makedirs(PATHS['topics-folder'])

    with open(topics_path + PATHS['topics-folder'], 'r+', encoding='utf-8') as topics_file:
        topics = topics_file.readlines()
        topics_file.close()

    with open(topics_path + PATHS['topics-filter'], 'w+', encoding='utf-8') as topics_file:
        for topic in topics:
            word_list = topic.split(':')[1]
            words = word_list.split(',')
            words_trimmed = [word.strip() for word in words if not 'kube' in word]
            if 'cpu' in words_trimmed or 'memory' in words_trimmed or 'http' in words_trimmed:
                topics_file.write(str(words_trimmed)+'\n')
                continue
            if 'error' in words_trimmed :
                for word in words_trimmed:
                    if 'fail' in word:
                        topics_file.write(str(words_trimmed)+'\n')
                        break

def count_topics():
    """ 
    The following script will count the words contained in topics.
    Raw topics must be in a file 'topics.txt' in the root of script directory.
    The count will be placed in a file 'topics_count.json' in the root of script directory
    
    Parameters
     ----------
      
    Returns
     -------
    """

    topics_path = PATHS['topics-folder']

    with open(topics_path + PATHS['topics'], 'r+', encoding='utf-8') as topics_file:
        topics = topics_file.readlines()
        topics_file.close()

    topics_word_count = {}

    for topic in topics:
        word_list = topic.split(':')[1]
        words = word_list.split(',')
        words_trimmed = [word.strip() for word in words]
        for word in words_trimmed:
            if word in topics_word_count:
                topics_word_count[word] += 1
            else:
                topics_word_count[word] = 1

    sorted_count = {word: count for word, count in sorted(topics_word_count.items(), key=lambda item: item[1], reverse=True) if not word.isdigit()}

    with open(topics_path + PATHS['topics-count'], 'w+', encoding='utf-8') as json_file:
        json.dump(sorted_count, json_file, indent=4)
        json_file.close()

def compute_lda(log, input_path, output_path):
    """ Performs the data cleaning to prepare for LDA
    
    Parameters
     ----------
     log : log buffer
     input_path : path of input file
     output_path : path of output file
      
    Returns
     -------
     None
    """

    logger.log_message(log, 'Preparing data for LDA execution...')

    with open(input_path, "r+", encoding="utf-8") as input_file:
        input_pd = pd.read_csv(input_file, dtype={'Id' : str, 'QuestionId': str, 'Title': str, 'Body': "object"})
        input_file.close()

    logger.log_message(log, 'Lemmatizing words...')

    # Apply lemmatization to tokens
    input_pd['Body'] = input_pd['Body'].map(lambda sentence: lemmatizing_sentence(eval(sentence)))

    # Get list instead of table
    tokens = input_pd['Body'].tolist()

    logger.log_message(log, 'Building bi/trigrams...')

    # Get 2-grams and 3-grams
    bigram_model = models.Phrases(tokens)
    trigram_model = models.Phrases(bigram_model[tokens], min_count=1)
        
    tokens = list(trigram_model[bigram_model[tokens]])

    logger.log_message(log, 'Building the corpus...')

    # Build a dictionary on tokens and discard all tokens with occurrences < 3
    dictionary_LDA = corpora.Dictionary(tokens)
    dictionary_LDA.filter_extremes(no_below=3)

    # Build the corpus
    corpus = [dictionary_LDA.doc2bow(tok) for tok in tokens]

    logger.log_message(log, 'Modeling topics with LDA...')

    # Set parameters
    np.random.seed(123456)
    topics = 20

    # Fit the LDA model
    lda_model = models.LdaModel(corpus, num_topics=topics,
                                id2word=dictionary_LDA,
                                passes=4, alpha=[0.01]*topics,
                                eta=[0.01]*len(dictionary_LDA.keys()))

    # Print the topics found by the LDA model
    with open(output_path + PATHS['topics'], "w+", encoding="utf-8") as output:
        print_topics_corpora(lda_model,LDA_INPUT['topics'], output)
        output.close()

    logger.log_message(log, 'Topics computed with Corpora library and dumped to ' + output_path + PATHS['topics'] + '.')