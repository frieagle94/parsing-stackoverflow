# Import useful system modules
import datetime

def log_message(log, message):
    """ Log the message and flush the buffer
    
    Parameters
     ----------
     log: log buffer
     message : Message to log
      
    Returns
     -------
     None
    """
    log.write(datetime.datetime.now().strftime('<%d/%m/%Y - %H:%M:%S>') + ' ' + message + '\n')
    log.flush()