SELECT Id, Title, Body, AcceptedAnswerId, null AS ParentId, null AS QuestionId
FROM Posts
WHERE AcceptedAnswerId IS NOT NULL AND Tags LIKE '%<kube%'
UNION
SELECT a.Id, NULL AS Title, a.Body, NULL AS AcceptedAnswerId, q.Id AS ParentId,
null AS QuestionId
FROM Posts q JOIN Posts a ON q.Id = a.ParentId 
WHERE a.ParentId in (SELECT Id FROM Posts WHERE AcceptedAnswerId IS NOT NULL AND Tags LIKE '%<kube%')
AND a.Id != q.AcceptedAnswerId
UNION
SELECT a.Id, NULL AS Title, a.Body, NULL AS AcceptedAnswerId, NULL AS ParentId,
q.Id AS QuestionId
FROM Posts q JOIN Posts a ON q.AcceptedAnswerId = a.Id 
WHERE q.Id in (SELECT Id FROM Posts WHERE AcceptedAnswerId IS NOT NULL AND Tags LIKE '%<kube%')
ORDER BY Id ASC