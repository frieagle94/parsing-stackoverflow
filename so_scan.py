# Import useful system modules
import re
import os
import sys
import json
import string
import traceback

# Import useful library modules
import pandas as pd

# Import custom modules
import logger
import lda
from config import SETTINGS, STEPS, PATHS, HEARTBEAT, CLUSTERING_SETTINGS, LDA_SETTINGS, read_settings

def clustering_topics(log):
    """ Performs the clustering on thread dataset
    
    Parameters
     ----------
     log : log buffer
      
    Returns
     -------
     None
    """
    import clustering
    import numpy as np

    logger.log_message(log, 'Start of clustering execution...')

    thread_paths = PATHS['cleaning-folder'] + PATHS['threads-cleaned-for-dumping-json']
    embedded_path = PATHS['embedding-folder'] + PATHS['threads-embedded-dataset']
    
    if not os.path.exists(thread_paths):
        logger.log_message(log, "Cleaned data do not exist. Execution will be aborted.")
        sys.exit(0)

    if not os.path.exists(embedded_path):
        logger.log_message(log, "Embedded data do not exist. Execution will be aborted.")
        sys.exit(0)

    with open(thread_paths, "r+", encoding="utf-8") as thread_file:
        threads_for_dumping = json.load(thread_file)
    
    X = np.loadtxt(open(embedded_path, "r+", encoding="utf-8"), delimiter=";", skiprows=1)
    K = best_K = int(SETTINGS['k'])

    if CLUSTERING_SETTINGS['pca-reduction']:
        X = clustering.pca_reduct(log, X, int(SETTINGS['main-components']))

    if CLUSTERING_SETTINGS['k-analysis']:
        best_K = clustering.k_analysys(log, X, int(SETTINGS['max-clusters']))

    clusters = None
    if CLUSTERING_SETTINGS['k-means']:
        if SETTINGS["use-best-k"]:
            K = best_K
        clusters = clustering.compute_clusters(log, X, K)

    if CLUSTERING_SETTINGS['dump-clusters']:

        clusters_to_dump_name = PATHS['clusters-to-dump']

        if clusters_to_dump_name == "all":
            for filename in os.listdir(PATHS['clusters-folder']):
                if filename.startswith("cluster_") and filename.endswith(".json"):
                    clustering.dump_threads_of_cluster(log, filename, threads_for_dumping, clusters)
        else:
            clustering.dump_threads_of_cluster(log, clusters_to_dump_name, threads_for_dumping, clusters)

    if CLUSTERING_SETTINGS['dump-stats-clusters']:
        
        import cleaner

        logger.log_message(log, 'Retrieving posts...')

        threads_to_clean_path  = PATHS['parsing-result-folder'] + PATHS['threads-flattened']

        with open(threads_to_clean_path, "r+", encoding="utf-8") as threads_to_clean_file:
            threads_pd = pd.read_csv(threads_to_clean_file)

        additional_stopwords = ['name', 'kubernetes', 'kubectl']

        cleaned_path = PATHS['cleaning-folder'] + 'additional_' + PATHS['threads-cleaned']

        # Recompute cleaned threads if not existing
        if not os.path.exists(cleaned_path):
            # Get tokens of threads
            input_pd = cleaner.clean_data_for_embedding(log, threads_pd, ['Body'], ['Id'], additional_stopwords)
        else:
            input_pd = pd.read_csv(cleaned_path, sep=',')
            input_pd['Body'] = input_pd['Body'].apply(lambda row: eval(row))
     
        clusters_to_stats_name = PATHS['clusters-to-stats']

        if clusters_to_stats_name == "all":
            for filename in os.listdir(PATHS['clusters-folder']):
                if filename.startswith("cluster_") and filename.endswith(".json"):
                    clustering.dump_stats_of_cluster(log, filename, input_pd, clusters)
        else:
            clustering.dump_stats_of_cluster(log, clusters_to_stats_name, input_pd, clusters)

    if CLUSTERING_SETTINGS['dump-stats-filtered-clusters']:

        clusters_stats_name = PATHS['clusters-to-stats']

        if clusters_stats_name == "all":
            for filename in os.listdir(PATHS['clusters-folder']):
                if filename.startswith("cluster_") and filename.endswith(".json"):
                    clustering.dump_filtered_stats_of_cluster(log, filename, threads_for_dumping, clusters)
        else:
            clustering.dump_filtered_stats_of_cluster(log, clusters_stats_name, threads_for_dumping, clusters)

def recreate_dirs(log):
    """ Recreate dirs
    
    Parameters
     ----------
     log : log buffer
      
    Returns
     -------
     None
    """
    for path in PATHS.values():
        if '/' in path:
            if not os.path.exists(path):
                os.makedirs(path)
                logger.log_message(log, "Directory " + path + " does not exist. It will be created.")

def main():

    with open(PATHS['log'], 'w+', encoding='utf-8') as log:

        log.write("Welcome to StackOverflow Scan.\n")
        log.write("\n")
        logger.log_message(log, "Config settings has been read.")
        log.flush()

        recreate_dirs(log)

        try:
            if STEPS['rebuild-base-data']:
                logger.log_message(log, "Going to recovering data due to config settings..")
                import thread_building as builder
                import cleaner
                import embedding as embed
                threads = builder.build_threads(log)
                threads_pd = builder.flatten_threads(log, threads)
                threads_cleaned_pd = cleaner.clean_data_for_embedding(log, threads_pd, ['Body'])
                cleaner.clean_dict_for_dumping(log, threads)
                embed.thread_embed(log, threads_cleaned_pd)
                logger.log_message(log, "Data recovered...")
            if STEPS['lda']:
                lda_input_path = PATHS['cleaning-folder'] + os.sep + PATHS['threads-cleaned']
                if not os.path.exists(lda_input_path):
                    logger.log_message(log, "Cleaned data do not exist. Execution will be aborted.")
                    sys.exit(0)
                lda_output_path = PATHS['topics-folder']
                if LDA_SETTINGS['modeling']:
                    lda.compute_lda(log, lda_input_path, lda_output_path)
                if LDA_SETTINGS['counting']:
                    lda.count_topics()
                if LDA_SETTINGS['filtering']:
                    lda.filter_topics()
            if STEPS['clustering']:
                clustering_topics(log)
        except Exception as e:
            print(str(e))
            logger.log_message(log, str(e))
            print(traceback.format_exc())
            logger.log_message(log, traceback.format_exc())
            raise
        logger.log_message(log, "End of script execution.")
        log.close()

if __name__ == "__main__":
    read_settings()
    main()