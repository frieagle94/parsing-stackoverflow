# Import useful system modules
import os
import json

# Import useful library modules
import html2text as html
import pandas as pd

# Import custom modules
import logger
from config import PATHS, HEARTBEAT, SETTINGS

def build_threads(log):
    """ Performs the tag filtering operation
    
    Parameters
     ----------
     log : log buffer
      
    Returns
     -------
     threads: threads dict
    """

    posts_path = PATHS['parsing-input-folder'] + PATHS['posts']
    output_path = PATHS['parsing-result-folder'] + PATHS['threads']

    if not os.path.exists(PATHS['parsing-result-folder']):
        os.makedirs(PATHS['parsing-result-folder'])
        logger.log_message(log, "Directory " + PATHS['parsing-result-folder'] + " does not exist. It will be created.")

    with open(posts_path, "r+", encoding="utf-8") as posts_input:
        input_posts = pd.read_csv(posts_input, delimiter=",", dtype={'Id' : str, 'Title': str, 'Body': str, 'ParentId': str, 'QuestionId': str, 'AcceptedAnswerId': str})
        posts_input.close()

    # Parse HTML Body
    input_posts['Body'] = input_posts['Body'].map(lambda x: html.html2text(x))

    logger.log_message(log, 'Start of threads parsing execution.')

    already_parsed_id = []
    threads = {}
    threads_rebuilt = 0
    
    for index, elem in input_posts.iterrows():
        
        post_id = elem.get('Id')
        
        if not post_id in already_parsed_id:

            replies = input_posts.loc[(input_posts['ParentId'] == post_id) & (input_posts['Id'] != elem.get('AcceptedAnswerId'))]
            accepted_answer = input_posts.loc[(input_posts['QuestionId'] == post_id) & (input_posts['Id'] == elem.get('AcceptedAnswerId'))]
            
            post = {
                'Id': elem.get('Id'),
                'Title': elem.get('Title'),
                'Body': elem.get('Body'),
            }
            
            childrens_ids, childrens = build_thread_childrens(replies, accepted_answer)
            
            already_parsed_id.append(post_id)
            
            if len(childrens_ids) > 0:
                already_parsed_id += childrens_ids
                post['Children'] = childrens
                
            threads[str(threads_rebuilt)] = post
            threads_rebuilt += 1
            
        # Write heartbeat on log
        if index > 0 and index%HEARTBEAT['rebuild-base-data'] == 0 :
            logger.log_message(log, 'Heartbeat: ' + str(index) + ' posts parsed. ' + str(threads_rebuilt) + ' threads rebuilt.')

    with open(output_path, 'w+') as res:
        json.dump(threads, res, indent = 4)
        res.close()

    logger.log_message(log, 'End of parsing execution: ' + str(threads_rebuilt) + ' rebuilt threads has been dumped to ' + PATHS['threads'])
    
    return threads

def build_thread_childrens(replies, accepted_answer):
    """ Recover the thread by recursively get childrens of post
    
    Parameters
     ----------
     replies: question's replies
     accepted_answer: question's answer
      
    Returns
     -------
     already_parsed_id: children ids
     children: children as json
    """

    if len(replies.index) == 0:
        if len(accepted_answer.index) == 1:
            return [accepted_answer.iloc[0]['Id']], accepted_answer.iloc[0]['Body']
        else:
            return [], ""
    else:
        reply = replies.iloc[0]
        children = {
            'Body': html.html2text(reply.get('Body'))
        }
        reply_id = reply.get('Id')

        childrens_ids, childrens = build_thread_childrens(replies[1:], accepted_answer)

        already_parsed_ids = [reply_id]
        already_parsed_ids += childrens_ids
        children['Children'] = childrens

        return already_parsed_ids, children

def flatten_threads(log, threads):
    """ Flatten the thread by recursively get childrens of post
    
    Parameters
     ----------
     log: log buffer
     threads: threads dict
      
    Returns
     -------
     flattened_thread: flattened threads DF
    """

    logger.log_message(log, 'Start of threads flattening execution.')

    output_path = PATHS['parsing-result-folder'] + PATHS['threads-flattened']

    flattened_thread = pd.DataFrame(columns=['Id', 'QuestionId', 'Title', 'Body'])

    for thread in threads:
        
        thread_json = threads[thread]
        flattened_thread.loc[int(thread)] = [thread, thread_json['Id'], thread_json['Title'], thread_json['Body'] + flatten_thread_childrens(thread_json['Children'])]

        # Write heartbeat on log
        if int(thread) > 0 and int(thread)%HEARTBEAT['rebuild-base-data'] == 0 :
            logger.log_message(log, 'Heartbeat: ' + thread + ' threads flattened.')

    with open(output_path, "w+", encoding="utf-8") as output:
        flattened_thread.to_csv(output)
        output.close()

    logger.log_message(log, 'End of flattening execution: flattened threads has been dumped to ' + PATHS['threads-cleaned-flattened'])

    return flattened_thread

def flatten_thread_childrens(childrens):
    """ Flatten thread's childrens by recursively get their childrens
    
    Parameters
     ----------
     childrens: childrens obj
      
    Returns
     -------
     anonymous: childrens flattened as string
    """
    if type(childrens) is str:
        return childrens
    else:
        return childrens['Body'] + flatten_thread_childrens(childrens['Children'])