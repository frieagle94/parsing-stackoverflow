# Import useful system modules
import copy

# Import custom modules
from config import TOPICS_MATCH_CONDITION

def check_words_matching(words):
    """ Check if words match the topic matching condition from config
    
    Parameters
     ----------
     words : list of words to check
      
    Returns
     -------
     T/F
    """
    
    conditions = copy.deepcopy(TOPICS_MATCH_CONDITION)

    for condition in conditions.values():
        if check_single_condition(words, condition):
            return True

    return False
        
def check_single_condition(words, conditions):
    """ Check if words match a single condition of or conditions
    
    Parameters
     ----------
     words : list of words to check
     condition : list of tuples in and condition

    Returns
     -------
     T/F
    """

    for word in words:
        if len(conditions) == 0:
            return True
        condition_matching = False
        for index_cond, condition in enumerate(conditions):
            condition_matching = check_word_matching(word, condition)
            if condition_matching:
                break
        if condition_matching:
            conditions.pop(index_cond)
    
    return len(condition) == 0
    
def check_word_matching(word, condition):
    """ Check if word matches the tiniest condition unit
    
    Parameters
     ----------
     words : list of words to check
     condition : single condition on word

    Returns
     -------
     T/F
    """

    operator = condition[0]
    topic = condition[1]
    
    if operator == 'whole':
        return (word == topic)
    elif operator == 'start':
        return word.startswith(topic)
    elif operator == 'sub':
        return topic in word
    else:
        return False